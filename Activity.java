package com.zuitt.example;

import java.util.Scanner;
public class Activity {

    public static void main(String[] args){
        String lastName, firstName;
        double firstSubject, secondSubject,thirdSubject,aveGrades;

        Scanner obj = new Scanner(System.in);

        System.out.println("First Name:");
        firstName= obj.nextLine();

        System.out.println("Last Name:");
        lastName= obj.nextLine();

        System.out.println("First Subject Grade:");
        firstSubject= obj.nextDouble();

        System.out.println("Second Subject Grade:");
        secondSubject= obj.nextDouble();

        System.out.println("Third Subject Grade:");
        thirdSubject= obj.nextDouble();

        System.out.println("Good day, " + firstName + " " + lastName);
      
        aveGrades = Math.round(firstSubject+secondSubject+thirdSubject)/3;
        int grades = (int) aveGrades;
        System.out.println("Your grade average is: " + grades);
    }
}
