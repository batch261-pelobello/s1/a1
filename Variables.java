//A "packege" in Java is used to group related classes. Think of it as a folder in directory
// Packages are divided into two categories
    //1. Built-in Packages - (packages from JAVA API)
    //2. User-defined Packages
// Package create follow the "reverse domain name notation" for the naming convention
package com.zuitt.example;

public class Variables {
    public static  void main(String[] args){
        //Variables
        // ";" delimiter - use to terminate a statement
        // Camel casing for declaring variable name para d malito si java
        int age;
        char middleInitial;

        // Variable Declaration vs Initialization
        // "=" assignment operator
        int x;
        int y = 0;
        //Initialization after declaration
        x = 1;

        //Output in the terminal
        System.out.println("The value of y is " + y + " and the value of x is " + x);

        //Primitive Data Types
        //predefined within the java programming which is used for a "single-valued" variables with limited capabilities

        //int - whole number values : 3bytes size
        int wholeNumber = 100;
        System.out.println(wholeNumber);

        //long - 4bytes size
        //L is being added at the end of the long number to be recognized
        long worldPopulation = 78974564984678465L;
        System.out.println(worldPopulation);

        //float
        //add f at the end of the flot to be recognized
        float piFloat = 3.141597654847f;
        System.out.println(piFloat);

        //double
        double piDouble = 3.141597654847;
        System.out.println(piDouble);

        //char - character; single character
        // uses single quote ''
        char letter = 'a';
        System.out.println(letter);

        //boolean true or false
        boolean isLove = true;
        boolean isTaken = false;
        System.out.println(isLove);
        System.out.println(isTaken);

        isTaken = true;

        //Constants - the value given to the variable cannot be change
        // final use before declaring the data type of the variable
        //making sure lang yung variable na constant kaya all caps
        final int PRINCIPAL = 3000;
        System.out.println(PRINCIPAL);
        //PRINCIPAL = 3000;

        //Non-primitive data type
            //also known as reference data types refer to instances for objects
        //String
        //Stores a sequences or array of characters
        //String are actually object that can use methods
        String userName= "JSmith";
        System.out.println(userName);

        int stringLength = userName.length();
        System.out.println(stringLength);

    }
}
