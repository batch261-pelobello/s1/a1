package com.zuitt.example;

import java.util.Scanner;
public class Userinput {
    public static void  main(String[] args){
        //instantiate
        //Scanner use to obtain an input from the terminal
        //Scanner is ued for obtaining input from the terminal
        //"System.in" allows us to take the input from the console
        Scanner myObj = new Scanner(System.in);
        System.out.println("Enter Username:");

        //To capture the input given by the user, we use the nextLine() method
        String userName = myObj.nextLine();
        System.out.println("Username is: " + userName);



    }
}
